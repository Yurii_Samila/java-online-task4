package Collection_tasks.Comparing_task;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.add(new Country("Italy", "Rome"));
        countries.add(new Country("Ukraine", "Kyiv"));
        countries.add(new Country("Spain", "Madrid"));
        countries.add(new Country("Germany", "Berlin"));
        countries.add(new Country("France", "Paris"));
        countries.add(new Country("Portugal", "Lisbon"));
        countries.add(new Country("England", "London"));
        countries.add(new Country("USA", "Washington"));
        countries.add(new Country("Brasil", "Brasilia"));
        countries.add(new Country("China", "Beijing"));
        Random random = new Random();

        Country country = new Country("Italy","Rome");

        Country countriesArray[] = new Country[5];
        for (int i = 0; i < countriesArray.length; i++) {
            countriesArray[i] = countries.get(random.nextInt(countries.size()));
        }
        ArrayList<Country> countryArrayList = new ArrayList<Country>();
        for (int i = 0; i < countriesArray.length; i++) {
            countryArrayList.add(countries.get(random.nextInt(countries.size())));

        }
        Arrays.sort(countriesArray);
        Collections.sort(countryArrayList);
        System.out.println(Arrays.toString(countriesArray));
        System.out.println(countryArrayList);
        System.out.println("-------------------------------------------------");
        Comparator<Country> comparator = (o1, o2) -> o1.getCapital().compareTo(o2.getCapital());
        Arrays.sort(countriesArray,comparator);
        Collections.sort(countryArrayList, comparator);
        System.out.println(Arrays.toString(countriesArray));
        System.out.println(countryArrayList);
        System.out.println("------------------------------------");

        int index = Arrays.binarySearch(countriesArray, country.getCapital());
        System.out.println(countriesArray[index]);

    }
}
