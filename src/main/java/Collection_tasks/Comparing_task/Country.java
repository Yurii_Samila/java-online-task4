package Collection_tasks.Comparing_task;

public class Country implements Comparable<Country> {
    private String country;
    private String capital;

    public Country(String country, String capital) {
        this.country = country;
        this.capital = capital;
    }



    @Override
    public String toString() {
        return "Country{" +
                "country='" + country + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }

    @Override
    public int compareTo(Country o) {
        return this.country.compareTo(o.country);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}
