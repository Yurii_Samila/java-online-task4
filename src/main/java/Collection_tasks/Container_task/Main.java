package Collection_tasks.Container_task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Container container = new Container();
        String[] arr = container.getArr();
        arr = container.add(arr, "AAA");
        arr = container.add(arr, "BBB");
        arr = container.add(arr, "CCC");
        System.out.println(Arrays.toString(arr));
        System.out.println(container.get(arr, 2));
        List<String> list = new ArrayList<String>();
        list.add("DDD");
        list.add("FFF");
        list.add("EEE");
        System.out.println(list);


    }
}
