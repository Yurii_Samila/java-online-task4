package Collection_tasks.Container_task;

import java.util.Arrays;

public class Container {
    private String[] arr = new String[0];

    static String[] add(String[] array, String st){
        array = Arrays.copyOf(array,array.length + 1);
        array[array.length-1] = st;
        return array;
    }
    static String get(String[] array, int index){
        return array[index];
    }


    public String[] getArr() {
        return arr;
    }

    public void setArr(String[] arr) {
        this.arr = arr;
    }

    @Override
    public String toString() {
        return "Container{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }
}


