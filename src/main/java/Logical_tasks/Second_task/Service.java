package Logical_tasks.Second_task;

public class Service {
    public static int[] repeating(int[] array, int maxNumberRepeat) {
        Counter[] counters = crateCounter(array);
        int[] tempArray = new int[array.length];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            for (Counter counter : counters) {
                if (counter.getValue() == array[i] && counter.getCount() < maxNumberRepeat) {
                    counter.setCount(counter.getCount() + 1);
                    tempArray[index++] = counter.getValue();
                }
            }
        }
        return trim(tempArray, index);
    }

    public static int[] distinct(int[] array) {
        int[] tempArray = new int[array.length];
        int j;
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            for (j = 0; j < i; j++) {
                if (array[i] == array[j]) {
                    break;
                }
            }
            if (i == j) {
                tempArray[index++] = array[i];
            }
        }
        return trim(tempArray, index);
    }

    public static int[] trim(int[] array, int newSize) {
        int[] newArray = new int[newSize];
        System.arraycopy(array, 0, newArray, 0, newArray.length);
        return newArray;
    }

    public static void print(String message, int[] values) {
        System.out.print(message);
        for (int item : values) {
            System.out.print(" " + item);
        }
        System.out.println();
    }

    private static Counter[] crateCounter(int[] array) {
        int[] uniqueArray = distinct(array);
        Counter[] counter = new Counter[uniqueArray.length];
        for (int i = 0; i < uniqueArray.length; i++) {
            counter[i] = new Counter(uniqueArray[i]);
        }
        return counter;
    }

}

