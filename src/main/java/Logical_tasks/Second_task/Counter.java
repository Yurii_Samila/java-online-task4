package Logical_tasks.Second_task;

public class Counter {
    private int value;
    private int count;

    public Counter() {
    }

    public Counter(int value) {
        this.value = value;
    }

    public Counter(int value, int count) {
        this.value = value;
        this.count = count;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
