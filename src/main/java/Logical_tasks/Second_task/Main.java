package Logical_tasks.Second_task;

import static Logical_tasks.Second_task.Service.repeating;
import static Logical_tasks.Second_task.Service.print;

public class Main {

    public static void main(String[] args) {
        int[] array = {1, 88, 77, 5, 6, 5, 22, 5, 88, 2,23};
        print("filtered array:", repeating(array, 2));
    }
}
