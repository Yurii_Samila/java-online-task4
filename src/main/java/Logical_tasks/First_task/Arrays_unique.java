package Logical_tasks.First_task;

public class Arrays_unique {
    public static int[] trim(int[] array, int newSize) {
        int[] newArray = new int[newSize];
        System.arraycopy(array, 0, newArray, 0, newArray.length);
        return newArray;
    }
    private static int[] findUnique(int[] arr1, int[] arr2){
        int[] temp = new int[Math.max(arr1.length, arr2.length)];
        int i = 0;
    for (int elem1 : arr1) {
        int u = 0;
        for (int elem2 : arr2) {
            if (elem1 != elem2){
                u++;
            }
        }
        if (u == arr2.length){
            temp[i++] = elem1;
        }
    }
    return trim(temp, i);
}
    public static int[] unique(int[] arr1, int[] arr2){
        int arrOne[] = findUnique(arr1, arr2);
        int arrTwo[] = findUnique(arr2, arr1);
        return concat(arrOne, arrTwo);
    }
    public static int[] concat(int[] arrOne, int[] arrTwo){
        int array[] = new int[arrOne.length + arrTwo.length];
        System.arraycopy(arrOne,0,array,0,arrOne.length);
        System.arraycopy(arrTwo,0,array,arrOne.length,arrOne.length);
        return array;
    }
}
