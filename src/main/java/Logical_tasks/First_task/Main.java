package Logical_tasks.First_task;

import Logical_tasks.First_task.Arrays_unique;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int arr1[] = new int[]{1,5,8,6,3};
        int arr2[] = new int[]{5,4,3,7,2};
        int count = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]){
                    count++;
                }
            }
        }
        int arr3[] = new int[count];
        int k = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]){
                    arr3[k] = arr1[i];
                    k++;
                }
            }
        }
        System.out.println(Arrays.toString(arr3));
        int count1 = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] != arr2[j]){
                    count1++;
                }
            }
        }
        Arrays_unique arrays_unique = new Arrays_unique();
        int[] unique = arrays_unique.unique(arr1, arr2);
        System.out.println(Arrays.toString(unique));


    }
}
