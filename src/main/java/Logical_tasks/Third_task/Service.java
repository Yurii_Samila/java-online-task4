package Logical_tasks.Third_task;

public class Service {
    public static int [] deleteSame(int [] array){
        int current = array[0] - 1;
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if(current != array[i]) {
                current = array[i];
                array[index++] = array[i];
            }
        }
        return trim(array, index);
    }

    public static int[] trim(int[] array, int newSize) {
        int[] newArray = new int[newSize];
        System.arraycopy(array, 0, newArray, 0, newArray.length);
        return newArray;
    }

    public static void show(String message, int[] values) {
        System.out.print(message);
        for (int item : values) {
            System.out.print(" " + item);
        }
        System.out.println();
    }
}
