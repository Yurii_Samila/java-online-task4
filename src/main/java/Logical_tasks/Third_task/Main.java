package Logical_tasks.Third_task;

import static Logical_tasks.Third_task.Service.deleteSame;
import static Logical_tasks.Third_task.Service.show;

public class Main {
    public static void main(String[] args) {
        int [] array = {1,1,1,2,3,4,3,3,4,4,5,6,6,7,7,7,34,45,4,5,5,5,6,7,7,3,2,4,5,3,2,4,4,55,54,4,6,6,6,6,23,43,4,4,3};
        show("The array is:", deleteSame(array));
    }
}
