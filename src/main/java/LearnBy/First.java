package LearnBy;

import java.io.*;
import java.util.Stack;

public class First {

    private File file;

    public First(String filePath) {
        this.file = new File(filePath);
    }

    public Stack<String> readFromFile() throws IOException {
        Stack<String> stack = new Stack<>();
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(this.file))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                stack.push(line);
                line = bufferedReader.readLine();
            }
        }
        return stack;
    }

    public   void writeInFile(Stack stack) throws IOException {

        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(this.file))) {
            while (!stack.empty()){
                bufferedWriter.write((String) stack.pop() + "\n");
            }
        }

    }
}
