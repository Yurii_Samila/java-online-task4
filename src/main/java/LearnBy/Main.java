package LearnBy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;
public class Main {
    public static void main(String[] args) throws IOException {
        First firstTask = new First("firstTask");
        Second secondTask = new Second();
        Third thirdTask = new Third("thirdTask");

        Stack<String> stack;
        //1
        stack = firstTask.readFromFile();
        firstTask.writeInFile(stack);
        //2
        Integer integer = secondTask.getNumberReverse(234);
        System.out.println(integer);
        //3
        System.out.println(thirdTask.sortCollectionByLineLength());

        //4
        ArrayList<String> stringArrayList = thirdTask.writePoemInCollection();
        Collections.sort(stringArrayList);
        System.out.println(stringArrayList);
    }
}
