package Game_task;

import java.util.Comparator;
import java.util.Objects;

public class Enemy implements Comparator<Enemy> {
    private final int MIN = 5;
    private final int MAX = 100;
    private final int MULTIPLIER = MAX - MIN;
    private int power = (int)(Math.random() * (MULTIPLIER + 1)) + MIN;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int compare(Enemy o1, Enemy o2) {
        return o1.getPower() - o2.getPower();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enemy enemy = (Enemy) o;
        return power == enemy.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(power);
    }

    @Override
    public String toString() {
        return "Enemy{" +
                "power=" + power +
                '}';
    }
}
