package Game_task;

import java.util.Comparator;
import java.util.Objects;

public class Artifact implements Comparator<Artifact> {
    private final int MIN = 10;
    private final int MAX = 80;
    private final int MULTIPLIER = MAX - MIN;
    private int power = (int)(Math.random() * (MULTIPLIER + 1)) + MIN;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public int compare(Artifact o1, Artifact o2) {
        return o1.getPower() - o2.getPower();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artifact artifact = (Artifact) o;
        return power == artifact.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(power);
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "power=" + power +
                '}';
    }
}
